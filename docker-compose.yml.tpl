version: '3'
services:
  web:
    image: docker.io/nibatandukar/ssh-berry1:$TAG
    command: "node server.js"
    ports:
      - "9092:5000"
    links:
      - "mongo"
  mongo:
    image: "mongo"
    ports:
      - "27017:27017"
